workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

include:
  - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/container-scanning@$CI_COMMIT_SHA
    inputs:
      stage: test
      dependencies: [build-image]

stages: [build, test, release]

build-image:
  image: docker:24.0.2
  stage: build
  services:
    - docker:24.0.2-dind
  script:
    - docker build --tag $CI_REGISTRY_IMAGE/test-image --file Dockerfile .
    - docker login --username gitlab-ci-token --password $CI_JOB_TOKEN $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE/test-image
  rules:
    # add to merge request but not branch pipeline for merge request.
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH # the container_scanning job is only added in branch/MR pipelines.
    # Since container_scanning job doesn't run for $CI_COMMIT_TAG we don't build the image either.

container_scanning:
  variables:
    CS_IMAGE: $CI_REGISTRY_IMAGE/test-image

ensure-job-added:
  stage: test
  needs: []
  image: badouralix/curl-jq
  script:
    - |
      route="$CI_API_V4_URL/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs"
      count=`curl --silent $route | jq 'map(select(.name | contains("container_scanning"))) | length'`
      if [ "$count" != "1" ]; then
        exit 1
      fi
  rules:
    # add to merge request but not branch pipeline for merge request.
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_SERVER_HOST =~ /gitlab.com/
    - if: $CI_OPEN_MERGE_REQUESTS
      when: never
    # the container_scanning job is only added in branch/MR pipelines.
    - if: $CI_COMMIT_BRANCH && $CI_SERVER_HOST =~ /gitlab.com/
    # Since container_scanning job doesn't run for $CI_COMMIT_TAG we don't run this job either.


# Ensure that a project description exists, because it will be important to display
# the resource in the catalog.
check-description:
  stage: test
  needs: []
  image: badouralix/curl-jq
  script:
    - |
      route="$CI_API_V4_URL/projects/$CI_PROJECT_ID"
      desc=`curl --silent $route | jq '.description'`
      if [ "$desc" = "null" ]; then
        echo "Description not set. Please set a projet description"
        exit 1
      else
        echo "Description set"
      fi
  rules:
    # add to merge request but not branch pipeline for merge request.
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_SERVER_HOST =~ /gitlab.com/
    - if: $CI_OPEN_MERGE_REQUESTS
      when: never
    # the container_scanning job is only added in branch/MR pipelines.
    - if: ($CI_COMMIT_BRANCH || $CI_COMMIT_TAG) && $CI_SERVER_HOST =~ /gitlab.com/

# Ensure that a `README.md` exists in the root directory as it represents the
# documentation for the whole components repository.
check-readme:
  image: busybox
  needs: []
  stage: test
  script: ls README.md || (echo "Please add a README.md file" && exit 1)
  rules:
    # add to merge request but not branch pipeline for merge request.
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH # the container_scanning job is only added in branch/MR pipelines.
    - if: $CI_COMMIT_TAG

# If we are tagging a release with a specific convention ("v" + number) and all
# previous checks succeeded, we proceed with creating a release automatically.
create-release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG =~ /\d+/
  script: echo "Creating release $CI_COMMIT_TAG"
  release:
    tag_name: $CI_COMMIT_TAG
    description: "Release $CI_COMMIT_TAG of components repository $CI_PROJECT_PATH"
